const gulp = require('gulp')
const browserSync = require('browser-sync')
const injector = require('bs-html-injector')
const reload = browserSync.reload
const autoprefixer = require('gulp-autoprefixer')
const sourcemaps = require('gulp-sourcemaps')
const sass = require('gulp-sass')
const pug = require('gulp-pug')
const plumber = require('gulp-plumber')
const postcss = require('gulp-postcss')
const lost = require('lost')
const postPosition = require('postcss-position')
const imagemin = require('gulp-imagemin')


const webpack = require('webpack')
const webpackStream = require('webpack-stream')

const minify = require('gulp-minify')
const concat = require('gulp-concat')
const clean = require('gulp-clean')
const runSequence = require('run-sequence')
const gulpCopy = require('gulp-copy')
const csso = require('gulp-csso')
const rename = require('gulp-rename')
const critical = require('critical')

const path = {
  build: {
    html: 'build/',
    js: 'build/js/',
    css: 'build/css/',
    images: 'build/images/',
    fonts: 'build/fonts/',
    maps: 'build/maps/'
  },
  src: {
    html: 'src/',
    pug: 'src/pug/pages/*.pug',
    js: './src/js/**/*.js',
    sass: 'src/sass/**/*.scss',
    css: 'src/css/',
    maps: 'maps/',
    files: 'src/files/**/*',
    images: 'src/images/**/*.*',
    fonts: 'src/fonts/**/*.*'
  },
  watch: {
    html: 'src/pug/**/*.pug',
    js: 'src/js/**/*.js',
    sass: 'src/sass/**/*.scss'
  },
  clean: './build'
}


gulp.task('browserSync', function () {
  browserSync.use(injector)
  browserSync({
    server: {baseDir: path.src.html},
    notify: true,
    open: false,
    ghostMode: false
  })
})

gulp.task('js-concat', function () {
  return gulp.src([
    // 'node_modules/tiny-slider/dist/tiny-slider.js',
    // 'node_modules/nouislider/distribute/nouislider.min.js',
    // 'node_modules/wnumb/wNumb.js',
    // 'node_modules/jquery/dist/jquery.js',
    // 'node_modules/magnific-popup/dist/jquery.magnific-popup.js',
    // // 'node_modules/jquery.maskedinput/src/jquery.maskedinput.js',
    // 'node_modules/jQuery-viewport-checker/dist/jquery.viewportchecker.min.js',
    // 'node_modules/countup.js/dist/countUp.min.js',
    // 'node_modules/cleave.js/dist/cleave.min.js',
    // 'node_modules/cleave.js/dist/addons/cleave-phone.ru.js',
    path.src.js,
    '!./src/js/all*'
  ])
  // .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(concat('all.js'))
    .pipe(minify({
      ext: {
        min: '.min.js'
      }
    }))
    // .pipe(sourcemaps.write())
    .pipe(clean('./src/js/all.min.js'))
    .pipe(gulp.dest('./src/js/'))
})
gulp.task('js-clean', function () {
  return gulp.src('./src/js/all.js')
    .pipe(clean())
})

gulp.task('images-files', function () {
  return gulp.src('./src/files/**/*.{png,jpg,svg}')
    .pipe(imagemin([
      imagemin.optipng({optimizationLevel: 5}),
      imagemin.jpegtran({progressive: true}),
      imagemin.svgo()
    ]))
    .pipe(gulp.dest('./src/files/'))
})

gulp.task('images', function () {
  return gulp.src('./src/images/**/*.{png,jpg,svg}')
    .pipe(imagemin([
      imagemin.optipng({optimizationLevel: 5}),
      imagemin.jpegtran({progressive: true}),
      imagemin.svgo()
    ]))
    .pipe(gulp.dest('./src/images/'))
})

gulp.task('sass', function () {
  gulp.src([
    'node_modules/magnific-popup/src/css/main.scss',
    // 'node_modules/nouislider/distribute/nouislider.min.css',
    'node_modules/animate.css/animate.min.css',
    // 'node_modules/tiny-slider/dist/tiny-slider.css',
    'node_modules/slick-carousel/slick/slick.scss',
    // 'node_modules/slick-carousel/slick/slick-theme.scss',
    path.src.sass])
    .pipe(sourcemaps.init({loadMap: true}))
    .pipe(sass()).on('error', sass.logError)
    .pipe(postcss([
      lost(),
      postPosition()
      // autoprefixer()
    ]))
    .pipe(autoprefixer({
      browsers: ['last 3 versions'],
      cascade: false
    }))
    .pipe(concat('styles.css'))
    // .pipe(csso())
    .pipe(sourcemaps.write(path.src.maps))
    .pipe(gulp.dest(path.src.css))
    .pipe(reload({stream: true}))
})
gulp.task('pug', function buildHTML() {
  return gulp.src(path.src.pug)
    .pipe(plumber())
    .pipe(pug({
      // pretty: true
    }))
    .pipe(gulp.dest(path.src.html))
    .pipe(reload({stream: true}))
})

gulp.task('js', () => {
  return gulp.src('./src/js/components/app.js')
    .pipe(plumber())
    .pipe(webpackStream({
      mode: 'production',
      output: {
        filename: 'app.js'
      },
      module: {
        rules: [
          {
            test: /\.(js)$/,
            // exclude: /(node_modules)/,
            loader: 'babel-loader'
          }

        ]
      },

      plugins: [
        new webpack.ProvidePlugin({
          $: 'jquery/dist/jquery.min.js',
          jQuery: 'jquery/dist/jquery.min.js',
          'window.jQuery': 'jquery/dist/jquery.min.js'
        })
      ]

      // externals: {
      //   jquery: 'jquery/dist/jquery.js'
      //   // tns: 'tiny-slider',
      //   // viewportChecker: 'jQuery-viewport-checker',
      //   // mfp: 'magnific-popup'
      // }

      // 'node_modules/jquery/dist/jquery.js',
      // 'node_modules/magnific-popup/dist/jquery.magnific-popup.js',
      // 'node_modules/jQuery-viewport-checker/dist/jquery.viewportchecker.min.js',
    }))
    .pipe(gulp.dest('./src/js/'))
})


gulp.task('critical', function () {
  critical.generate({
    inline: true,
    base: './build/',
    src: 'index.html',
    css: './build/css/styles.css',
    // extract: true,
    dest: 'index.html',
    minify: true,
    width: 1920,
    height: 930
  })
})

gulp.task('clean', function () {
  return gulp.src('./build', {read: false})
    .pipe(clean())
})

gulp.task('cssmin', function () {
  return gulp.src('./src/css/styles.css')
    .pipe(csso())
    // .pipe(rename("styles.min.css"))
    .pipe(gulp.dest('./build/css'))
})

gulp.task('html-copy', function () {
  return gulp.src(['./src/*.html',
    './src/browserconfig.xml',
    './src/site.webmanifest',
    './src/policy.pdf'])
    .pipe(gulp.dest('./build/'))
})

gulp.task('files-copy', function () {
  return gulp.src(['./src/files/**/*', '!README.md'])
    .pipe(gulp.dest('./build/files'))
})

gulp.task('images-copy', function () {
  return gulp.src(['./src/images/**/*', '!README.md'])
    .pipe(gulp.dest('./build/images'))
})

gulp.task('fonts-copy', function () {
  return gulp.src(['./src/fonts/**/*', '!README.md'])
    .pipe(gulp.dest('./build/fonts'))
})

gulp.task('js-copy', function () {
  return gulp.src('./src/js/all.min.js')
    .pipe(gulp.dest('./build/js/'))
})

gulp.task('build', function () {
  runSequence('clean', 'sass', 'cssmin', 'pug', 'html-copy', 'files-copy', 'images-copy', 'fonts-copy', 'js', 'js-copy')
})


gulp.task('watch', function () {
  gulp.watch(path.watch.sass, function () {
    setTimeout(function () {
      gulp.start('sass')
    }, 100)
  })
  gulp.watch(path.watch.html, function () {
    setTimeout(function () {
      gulp.start('pug')
    }, 100)
  })
  gulp.watch([
    './src/js/components/*.js'
  ], () => {
    setTimeout(() => {
      gulp.start('js')
    }, 100)
  })
})
gulp.task('default', ['browserSync', 'sass', 'js', 'watch', 'pug'])
