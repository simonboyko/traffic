'use strict'



// import { WebpackBundleSizeAnalyzerPlugin } from 'webpack-bundle-size-analyzer';

module.exports = {
  mode: 'production',

  entry: __dirname + '/src/js/components/app.js',

  output: {
    filename: 'app.js',
    path: __dirname + '/src/js'

  },

  module: {
    rules: [
      {
        test: /\.(js)$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader'
      }

    ]
  },

  // plugins: [
  //   new webpack.ProvidePlugin({
  //     $: 'jquery/dist/jquery.min.js',
  //     jQuery: 'jquery/dist/jquery.min.js',
  //     'window.jQuery': 'jquery/dist/jquery.min.js'
  //   }),
  //   new WebpackBundleSizeAnalyzerPlugin('./plain-report.txt')
  // ]

  // externals: {
  //   jquery: 'jquery/dist/jquery.js'
  //   // tns: 'tiny-slider',
  //   // viewportChecker: 'jQuery-viewport-checker',
  //   // mfp: 'magnific-popup'
  // }

  // 'node_modules/jquery/dist/jquery.js',
  // 'node_modules/magnific-popup/dist/jquery.magnific-popup.js',
  // 'node_modules/jQuery-viewport-checker/dist/jquery.viewportchecker.min.js',
}