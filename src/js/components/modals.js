console.log('INIT modals.js')

import $ from 'jquery/dist/jquery'
import magnificPopup from 'magnific-popup/dist/jquery.magnific-popup'

$('[data-popup]').magnificPopup({
  type:'inline',
  midClick: true
});