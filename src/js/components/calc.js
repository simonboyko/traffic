'use strict'

// console.log('INIT calc.js')


let Calc = function () {

  let els = {
    inputSide: {},
    outputSide: {
      wrp: document.querySelector('.calc__main--we')
    },
    fieldClass: '.calc__value'
  }

  els.inputSide.wrp = document.querySelector('.calc__main--client')
  els.inputSide.rows = els.inputSide.wrp.querySelectorAll('.calc__row')
  els.outputSide.rows = els.outputSide.wrp.querySelectorAll('.calc__row')

  this.init = () => {

    checkFieldUpdate()
  }


  function checkFieldUpdate() {
    // console.log('f: checkFieldUpdate')

    els.inputSide.rows.forEach(row => {
      let fields = row.querySelectorAll(els.fieldClass)

      fields.forEach(f => {
        f.addEventListener('input', onInput)
      })


    })

    function onInput(e) {
      // console.log('f: onUpdate')

      let el = e.target
      let value = el.value
      let fid = el.getAttribute('data-fid')
      let sameValue = els.outputSide.wrp.querySelector(els.fieldClass + '[data-fid="' + fid + '"]')

      sameValue.innerHTML = value

      updateResults()

    }


    function updateResults() {
      // console.log('f: updateResults')

      let res = {
        orders: 0,
        price: 0,
        loss: 0
      }
      for (let i = 0; i < els.outputSide.rows.length; i++) {
        let row = els.outputSide.rows[i]

        let order = Number(row.querySelector('.calc__value--order').innerHTML)
        let price = Number(row.querySelector('.calc__value--price').innerHTML)

        res.orders = res.orders + order

        res.price = res.price + order * price

        if (i === 0) res.loss = order * price

      }

      // меняем значение для кол-ва заявок
      let resOrders = document.querySelectorAll('.calc__quantity')
      for (let i = 0; i < resOrders.length; i++) {
        resOrders[i].innerHTML = res.orders
      }

      // меняем значение для общей сумму
      let resPrice = document.querySelector('.calc__amount')
      resPrice.innerHTML = res.price

      // меняем значение для потеряных денег
      let resLoss = document.querySelector('.calc__loss')
      resLoss.innerHTML = res.loss
    }

  }
}


let calc = new Calc()
calc.init()
