'use strict'

console.log('INIT cases.js')

import {tns} from 'tiny-slider/src/tiny-slider'
import Slick from 'slick-carousel/slick/slick'
import $ from 'jquery/dist/jquery'
import magnificPopup from 'magnific-popup/dist/jquery.magnific-popup'


// Наши кейсы
// let caseSliderTop = tns({
//   container: '.slider__listTop',
//   items: 2,
//   slideBy: 1,
//   nav: false,
//   controlsContainer: '.cases__arrows',
//   responsive: {
//     759: {
//       items: 3,
//       slideBy: 2
//     }
//   }
// })

$(".slider__listTop").slick({

  slidesToShow: 2,
  mobileFirst: true,
  prevArrow: $('.slider__arrow--prev'),
  nextArrow: $('.slider__arrow--next'),
  rows: 2,

  responsive: [{

    breakpoint: 759,
    settings: {
      slidesToShow: 3
    }

  }]
});


console.log('INIT bars.js')

let caseLink = document.querySelectorAll('.slider__item')


$('[data-popup-case]').magnificPopup({
  type: 'inline',
  midClick: true,
  callbacks: {
    open: function () {

      setBars(this.currItem.inlineElement[0])

    },
    close: function () {
      unsetBars(this.currItem.inlineElement[0])
    }
  }
})


// initBars()




function initBars() {
  // выставляем прогрессбары в начальное
  let bars = document.querySelectorAll('.caseStats__bar')

  for (let i = 0; i < bars.length; i++) {

    bars[i].style.width = '0%'

  }


  console.log('f: initBars')
}


function setBars(caseEl) {
  // каждый блок сравнения
  let items = caseEl.querySelectorAll('.caseStats__achievement')


  setTimeout(() => {
    for (let i = 0; i < items.length; i++) {
      let offset = items[i].getAttribute('data-bar-position')
      let rows = items[i].querySelectorAll('.caseStats__row')

      for (let j = 0; j < rows.length; j++) {
        let bar = rows[j].querySelector('.caseStats__bar')
        let text = rows[j].querySelector('.caseStats__text')

        bar.style.width = offset + '%'
        text.style.marginLeft = offset + '%'
      }

    }
  }, 400)

  console.log('f: setBars')
}


function unsetBars(caseEl) {
  // выставляем прогрессбары в соответствии с атрибутом
  let bars = caseEl.querySelectorAll('.caseStats__bar')

  for (let i = 0; i < bars.length; i++) {

    bars[i].style.width = ''

  }

  console.log('f: unsetBars')
}

