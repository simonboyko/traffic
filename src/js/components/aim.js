'use strict';

console.log('INIT aim.js')

// кнопка "Прицелиться"
const button = document.querySelector('.goals__button')
// прицел
const aim = document.querySelector('.goals__aim')
// родительский элемент блока
const block = document.querySelector('.goals')




button.addEventListener('click', (e) => {
  console.log('e: button.click')

  followPointer(e)

  block.addEventListener('mousemove', followPointer)

  block.classList.add('goals--target')

})

let body = document.getElementsByTagName('body')[0]

body.addEventListener('click', (e) => {
  if (e.target != button) {

    block.removeEventListener('mousemove', followPointer)
    block.classList.remove('goals--target')

    aim.style.position = 'absolute'
    aim.style.zIndex = '1'
    aim.style.top = '50%'
    aim.style.left = '50%'

  }
})

function followPointer(e) {

  let x = e.clientX
  let y = e.clientY

  aim.style.position = 'fixed'
  aim.style.zIndex = '999'
  aim.style.top = y + 'px'
  aim.style.left = x + 'px'
}





