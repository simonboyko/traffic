console.log('INIT app.js')


import Modals from './modals'
import Cases from './cases'
import Aim from './aim'
import Animation from './animation'
import Calculator from './calc'


// Smooth scroll
import SmoothScroll from 'smooth-scroll/dist/smooth-scroll'
let scroll = new SmoothScroll('a[data-scroll]');


// Parallax
import Rellax from 'rellax/rellax'
let rellax = new Rellax('[data-parallax]', {
})




