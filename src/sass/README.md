# Your styles

The folder and the sub-folders contain styles. These are compiled from *.scss to *.css by the gulp into the /css/ folder. You don't have to edit *.css files directly, while they are generated automatically. All changes in the *.css files will be lost by the compiling.